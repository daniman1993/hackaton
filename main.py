import facebook
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import json
import requests
import urllib
import os
# Globals

# URLs
fb_login_url = "https://www.facebook.com/"
messages_url = "https://www.facebook.com/messages"

# Xpaths
email_input_xpath = r"/html[@id='facebook']/body[@class='fbIndex UIPage_LoggedOut _2gsg _xw8 _5p3y chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@id='pagelet_bluebar']/div/div[@id='blueBarDOMInspector']/div[@class='_4f7n _1s4v _26aw _hdd _xxp']/div/div[@class='loggedout_menubar_container']/div[@class='clearfix loggedout_menubar']/div[@class='menu_login_container rfloat _ohf']/form[@id='login_form']/table/tbody/tr[2]/td[1]/input[@id='email']"
pwd_input_xpath = r"/html[@id='facebook']/body[@class='fbIndex UIPage_LoggedOut _2gsg _xw8 _5p3y chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@id='pagelet_bluebar']/div/div[@id='blueBarDOMInspector']/div[@class='_4f7n _1s4v _26aw _hdd _xxp']/div/div[@class='loggedout_menubar_container']/div[@class='clearfix loggedout_menubar']/div[@class='menu_login_container rfloat _ohf']/form[@id='login_form']/table/tbody/tr[2]/td[2]/input[@id='pass']"
login_btn_xpath = r"/html[@id='facebook']/body[@class='fbIndex UIPage_LoggedOut _2gsg _xw8 _5p3y chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@id='pagelet_bluebar']/div/div[@id='blueBarDOMInspector']/div[@class='_4f7n _1s4v _26aw _hdd _xxp']/div/div[@class='loggedout_menubar_container']/div[@class='clearfix loggedout_menubar']/div[@class='menu_login_container rfloat _ohf']/form[@id='login_form']/table/tbody/tr[2]/td[3]/label[@id='loginbutton']/input[@id='u_0_y']"
message_id_list = "wmMasterViewThreadlist"
more_pages_id = "uiMorePagerPrimary"

start_conver_xpath = r"/html[@id='facebook']/body[@class='_5uj5 _4rw fbx noFooter _5p3y chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@id='globalContainer']/div[@id='content']/div/div[@id='mainContainer']/div[@id='contentCol']/div[@id='contentArea']/div[@id='pagelet_web_messenger']/div[@id='u_0_s']/div[@class='_ksg clearfix']/div[@class='_2nb']/div[@class='clearfix']/div[@class='_2nj']/div[@class='uiScrollableArea _2nc uiScrollableAreaWithTopShadow nofade uiScrollableAreaWithShadow uiScrollableAreaBottomAligned contentAfter']/div[@class='uiScrollableAreaWrap scrollable']/div[@class='uiScrollableAreaBody']/div[@class='uiScrollableAreaContent']/div/div[5]/div/ul[@id='webMessengerRecentMessages']/li[@class='_2n3'][1]/abbr[@class='timestamp']"
chat_box_xpath = r"/html[@id='facebook']/body[@class='_5uj5 _4rw fbx noFooter _5p3y chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@id='globalContainer']/div[@id='content']/div/div[@id='mainContainer']/div[@id='contentCol']/div[@id='contentArea']/div[@id='pagelet_web_messenger']/div[@id='u_0_s']/div[@class='_ksg clearfix']/div[@class='_2nb']/div[@class='clearfix']/div[@class='_2nj']/div[@class='uiScrollableArea _2nc uiScrollableAreaWithTopShadow nofade uiScrollableAreaWithShadow uiScrollableAreaBottomAligned contentAfter']/div[@class='uiScrollableAreaWrap scrollable']/div[@class='uiScrollableAreaBody']/div[@class='uiScrollableAreaContent']/div/div[5]"

# FB credentials
user = CHANGE_ME
pwd = CHANGE_ME

# Limits
POST_LIMIT = 50 # should be <=0 for ALL
CHAT_LIMIT = 5 # should be -1 for infinite
PHOTO_LIMIT = 70
class Helpers:
	def print_dict(self, d):
		for key in d:
			for key in d:
				print key
			
	def expected_fields_exists(self, obj, fields):
		for field in fields:
			if field not in obj:
				print "missing " + field
				return
		return True
		
	def parse_response(self, resp):
		if not resp or "data" not in resp:
			return
		else:
			return resp["data"]

# Class that gets oath from FB dev console
# (uses parents driver)
class Get_Oath():
	def check_if_logged_in(self):
		try:
			e = self.driver.find_element_by_id("loginbutton")
		except:
		#	print "Logged in"
			return True

		#print "Not logged in"
		return False
		

	def login_dev(self):
		# Login to facebook dev
		e = self.driver.find_element_by_name("email")
		e.send_keys(user)
		e = self.driver.find_element_by_name("pass")
		e.send_keys(pwd)
		e.send_keys(Keys.RETURN)
		
		

	def get_access_token_after_logon(self):
		# Open the 'get access token' window
		e = self.driver.find_element_by_xpath("/html[@id='facebook']/body[@class='_55w7 chrome webkit win x1 Locale_en_US']/div[@class='_li']/div[@class='_53vp']/div[@class='clearfix _53u5']/div[@class='_4bl7 _bh _53u7']/div[@id='u_0_0']/div[@class='_57mb _4-u2  _4-u8']/div[@class='_3a8w _4-u3']/div[@class='_371-']/div[@class='_1c2l uiPopover _6a _6b']/a[@class='_1c2m _p _55pi _2agf _4jy0 _4jy3 _517h _51sy _42ft']")
		e.send_keys(Keys.SPACE)

		e = self.driver.find_element_by_xpath("/html[@id='facebook']/body[@class='_55w7 chrome webkit win x1 Locale_en_US']/div[@id='js_4']/div[@class='uiContextualLayer uiContextualLayerBelowLeft']/div[@id='js_7']/div[@class='_54ng']/ul[@class='_54nf']/li[@class='_54ni _1sbv __MenuItem _54ne selected']/a[@class='_54nc']")
		e.send_keys(Keys.SPACE)

		# Check all the permissions
		l = self.driver.find_elements_by_class_name("uiInputLabelInput")
		for e in l:
			try:
				e.send_keys(Keys.SPACE)
			except:
				pass

		e = self.driver.find_element_by_xpath("/html[@id='facebook']/body[@class='_55w7 chrome webkit win x1 Locale_en_US']/div[@class='_10 uiLayer _4-hy _3qw']/div[@class='_59s7']/div[@class='_4t2a']/div/div/div/div[@class='_5a8u _5lnf uiOverlayFooter']/div[@class='_50f4']/div[@class='clearfix']/div[@class='_ohf rfloat']/div/div/button[@class='_4jy0 _4jy3 _4jy1 _51sy selected _42ft']")
		e.send_keys(Keys.SPACE)

		self.driver.switch_to_window(self.driver.window_handles[1])

		# In the window we are asked for permissions - press confirm.
		e = self.driver.find_element_by_name("__CONFIRM__")
		if e != None:
			e.send_keys(Keys.SPACE)

		while len(self.driver.window_handles) >= 2:
			try:
				e = self.driver.find_element_by_name("__CONFIRM__")
				if e != None:
					e.send_keys(Keys.SPACE)
			except:
				pass

			print self.driver.window_handles
			time.sleep(1)

		self.driver.switch_to_window(self.driver.window_handles[0])

		# Finally - get the access tokeb from the text box
		l = self.driver.find_elements_by_class_name("_58al")
		access_token = ""
		for e in l:
			try:
				val = e.get_attribute("value")
				if val != "":
					access_token = val
					break
			except:
				pass

		return access_token

	def init_oath(self):
		if self.driver == None:
			print "get_access_token: no driver"
			return None

		self.driver.get("http://developers.facebook.com/tools/explorer")

		if not self.check_if_logged_in():
			self.login_dev()

		access_token = self.get_access_token_after_logon()
		
		if not access_token:
			print "get_access_token failed"
			return
		self.oath = access_token
		return True
	

class FB_Chat():
	def get_chats(self):
		self.driver.get("https://www.facebook.com")
		if not self.check_if_logged_in():
			self.login_fb()
		
		links =  self.__get_links()
		chats = []
		for i, link in enumerate(links):
			if i == CHAT_LIMIT:
				break
			chat = self.get_chat(link)
			chats.append(chat)
		return chats
		
	def login_fb(self):
		self.driver.get(fb_login_url)
		email_input = self.driver.find_element_by_name("email")
		email_input.send_keys(user)
		pwd_input = self.driver.find_element_by_name("pass")
		pwd_input.send_keys(pwd)
		pwd_input.send_keys(Keys.RETURN)
		
	def __get_links(self):
		self.driver.get(messages_url)
		
		# Load all the chats (chats are loaded by pressing a 'load more' button)
		while True:
			try:
				e = self.driver.find_element_by_class_name(more_pages_id)
				e.send_keys(Keys.RETURN)
			except:
				break
			time.sleep(1)

		e = self.driver.find_element_by_id(message_id_list)
		l = e.find_elements_by_tag_name("li")
		links = []
		
		# Create a list contains the links for all the chats
		for child in l:
			chat_id = child.find_element_by_class_name("_k_")
			link =  chat_id.get_attribute("href")
			links.append(link)
			#print link
		return links

	def get_chat(self, link):
		self.driver.get(link)
		time.sleep(1)
		
		# Scroll to the top of the messages box in order to load all the messages
		scroll_pos1 = -1
		scroll_pos2 = -1
		while scroll_pos1 != 0 or scroll_pos2 != 0:
			self.driver.execute_script('document.getElementsByClassName("uiScrollableAreaWrap")[1].scrollTop = 0')
			self.driver.execute_script('document.getElementsByClassName("uiScrollableAreaWrap")[2].scrollTop = 0')
			time.sleep(3)
			scroll_pos1 = self.driver.execute_script('return document.getElementsByClassName("uiScrollableAreaWrap")[1].scrollTop')
			scroll_pos2 = self.driver.execute_script('return document.getElementsByClassName("uiScrollableAreaWrap")[2].scrollTop')
		
		list_e = self.driver.find_element_by_id("webMessengerRecentMessages")
		l = list_e.find_elements_by_class_name("webMessengerMessageGroup")
		
		messages = []
		
		# Go through the messages and extract author,date, and data
		for e in l:
			author_box = e.find_element_by_tag_name("strong").find_element_by_tag_name("a")
			author = author_box.text
		
			date_box = e.find_element_by_tag_name("abbr")
			date = date_box.text
			
			# Get all the text data for the same date
			texts = []
			e2 = e.find_element_by_class_name("_53")
			for x in e2.find_elements_by_class_name("_3hi"):
				try:
					texts.append(x.find_element_by_tag_name("p").text)
				except:
					pass
					
			# Get all the image date for the same date
			images = []
			e2 = e.find_element_by_class_name("_sq")
			image_url = None
			try:
				image_box = e2.find_element_by_class_name("_4yp9")
				image_url = image_box.value_of_css_property("background-image")[5:][:-2]
			except:
				pass
			
			if image_url != None:
				# 2D0: If there are 2 images for the same date - one will be overrided by the other
				image_name = "images/" + link[link.rfind("/") + 1:]+ "_" +  date.replace(" ","_").replace(",", "").replace("/", ".").replace(":", "-")  + ".jpg"
				urllib.urlretrieve(image_url, image_name)
				images.append(image_name)
				
					
			message = { 'author' : author,
						'date' : date,
						'texts' : texts,
						'images' : images }
						
			messages.append(message)
			
			print message
		# 2DO handle images and videos
		return messages
			

class FB_reader():
	graph = None
	profile = None
	
	def init(self):
		if not self.oath:
			print "FB_reader Init missing token"
			return
		if self.graph and self.profile: # already init
			return
		self.graph = facebook.GraphAPI(self.oath)
		if not self.graph:
			print "fb_reader __init__: failed getting graph"
			return
		user = 'me'
		self.profile = self.graph.get_object(user)
		if not self.profile:
			print "fb_reader __init__: failed getting profile"
			return
		#print_dict(self.profile)
			
		return True
		
		
	def __get_like_count(self, likes):
		count = 0
		if "data" not in likes:
			count
		count += len(likes["data"])
		
		while "paging" in likes and "next" in likes["paging"]:
			try:
				likes = requests.get(likes['paging']['next']).json()
				count += len(likes["data"])
				
			except KeyError:
				# When there are no more pages (['paging']['next']), break from the
				# loop and end the script.
				break
				
		return count
		
		
	def __parse_comments(self, comments):
		if not comments or "data" not in comments:
			return
		comment_list = []
		for comment in comments["data"]:
			expected_fields_in_comment = ["from", "like_count", "id", "message", "created_time"]
			if not self.expected_fields_exists(comment, expected_fields_in_comment):
				print "__parse_comments: missing expected field in comment"
				continue
			comment_d = {"id" : comment["id"], "from" : comment["from"], "like_count" : comment["like_count"], "msg" : comment["message"], "time" : comment["created_time"]}
			comment_list.append(comment_d)
		return comment_list
		
		
	def get_posts(self):
	# gets all posts without comments
		if not self.init():
			print "get_photos init failed"
			return
		all_posts = []
		posts = self.graph.get_connections(self.profile['id'], 'posts')
		if not posts or "data" not in posts:
			print "get_posts: no posts"
			return
		
		i = 1
		while True:
			try:
				for post in posts['data']:
					expected_fields_in_post = ["id", "created_time"]
					if not self.expected_fields_exists(post, expected_fields_in_post):
						print "get_posts: missing expected field in post"
						continue
					post_item = {"id" : post["id"], "time" : post["created_time"]}
					if "message" in post:
						post_item["msg"] = post["message"]
					if "story" in post:
						post_item["story"] = post["story"]
					if "comments" in post:
						post_item["comments"] = self.__parse_comments(post["comments"])
					if "likes" in post:
						post_item["like_count"] = self.__get_like_count(post["likes"])
					all_posts.append(post_item)
					
					i += 1
					if i == POST_LIMIT:
						return all_posts
				
				# Attempt to make a request to the next page of data, if it exists.
				posts = requests.get(posts['paging']['next']).json()
				
			except KeyError:
				# When there are no more pages (['paging']['next']), break from the
				# loop and end the script.
				break
		return all_posts
		
		

		
		
	def __get_photos(self):
		all_photos = []
		photos = self.graph.get_connections(self.profile['id'], 'photos')
		i = 0
		while True:
			try:
				if not self.parse_response(photos):
					print "__get_photos : bad response"
					break
				expected_fields_in_photo = ["link", "from", "created_time"]
				for photo in photos["data"]:
					if not self.expected_fields_exists(photo, expected_fields_in_photo):
						print "__get_photos: missing expected field in photo"
						continue
					photo_d = {"link" : photo["link"], "from" : photo["from"], "time" : photo["created_time"]}
					if "comments" in photo:
						comments = self.__parse_comments(photo["comments"])
						photo_d["comments"] = comments
						
					if "likes" in photo and "data" in photo["likes"]:
						photo_d["like_count"] = self.__get_like_count(photo["likes"])
					i += 1
					if i == PHOTO_LIMIT:
						return all_photos
					all_photos.append(photo_d)
				
				
				# Attempt to make a request to the next page of data, if it exists.
				photos = requests.get(photos['paging']['next']).json()
				
				
			except KeyError:
				# When there are no more pages (['paging']['next']), break from the
				# loop and end the script.
				break
			return all_photos
		
		
	def get_photos(self):
		if not self.init():
			print "get_photos init failed"
			return
		
		if not self.profile or "id" not in self.profile:
			print "get_posts: not profile"
			return
		photos = self.__get_photos()
		with open('photos.txt', 'w') as outfile:
			json.dump(photos, outfile)
		print photos
		return
		# gets all posts without comments
		all_photos = []
		photos = self.graph.get_connections(self.profile['id'], 'photos')
		photos = self.parse_response(photos)
		if not photos:
			print "bad response in get photos"
			return
		photos
		
		# {from,comments}
		#print photos
		return
		if not photos or "data" not in photos:
			print "get_photos: no posts"
			return
		
		i = 1
		while True:
			try:
				all_photos += photos['data']
				# Attempt to make a request to the next page of data, if it exists.
				photos = requests.get(posts['paging']['next']).json()
				
				for post in posts["data"]:
					print post
				i += 1
				if i == PHOTO_LIMIT:
					break
				
			except KeyError:
				# When there are no more pages (['paging']['next']), break from the
				# loop and end the script.
				break
		
class FB_Main(Helpers, Get_Oath, FB_Chat, FB_reader):
	driver = None
	username = ""
	pwd = ""
	oath = ""
	def __init__(self, user, pwd):
		self.driver = webdriver.Chrome('chromedriver')
		if not os.path.exists(r"\imagaes"):
			os.mkdir("images")
		else:
			for the_file in os.listdir(r"\imagaes"):
				file_path = os.path.join(r"\imagaes", the_file)
				try:
					if os.path.isfile(file_path):
						os.unlink(file_path)
					#elif os.path.isdir(file_path): shutil.rmtree(file_path)
				except Exception, e:
					print e
	def destructer(self):
		self.driver.quit()

def main():
	fb = FB_Main("user", "pwd")
	if not fb.init_oath():
		print "init_oath failed"
		return
		
		
	photos = fb.get_photos()
	with open('photos.txt', 'w') as outfile:
		json.dump(photos, outfile)

	posts = fb.get_posts()
	with open('posts.txt', 'w') as outfile:
		json.dump(posts, outfile)
	
	chats = fb.get_chats()
	with open('chats.txt', 'w') as outfile:
		json.dump(chats, outfile)
		
	print "good"
	#print fb.oath
	#time.sleep(15)
	fb.destructer()
	
		
main()